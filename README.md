﻿# Spike Report

## UMG UI Designer

### Introduction

We’ve come a long way already, with a little game prototype that handles most of the Unreal Gameplay Framework, and now we want to put in some polish into our game. 

Any gameplay-complete prototype needs a few things to make the User eXperience complete – and we’re going to start by learning how to make a HUD (Heads Up Display) and a Menu.


### Goals

1. Knowledge: What tools does Unreal Engine have to make UI with?
    - Hint: there have been several, but we’ll be mainly focused on the UMG UI Designer
1. Skill: How can we build a HUD and a Menu within Unreal?
1. Tech: How do we hook up a HUD and Menu to work within our game?


### Personnel

* Primary - Max Finn
* Secondary - N/A

### Technologies, Tools, and Resources used

* [UMG UI Designer Quick Start Guide](https://docs.unrealengine.com/latest/INT/Engine/UMG/QuickStart/index.html)
* [UMG UI Designer](https://docs.unrealengine.com/latest/INT/Engine/UMG/)
* [Blueprints, Creating Variables in C++ For Use In BP](https://wiki.unrealengine.com/Blueprints,_Creating_Variables_in_C%2B%2B_For_Use_In_BP)

### Tasks Undertaken

A near exact set of steps complete with ample screenshots can be found in the UMG UI Designer Quick Start Guide above. The following is a condensed equivalent.

1. Create Widget Blueprints for the HUD, Main Menu, and Pause Menu. Within each, set up the hierarchy of elements (horizontal/vertical boxes, borders, buttons, text) as required.
1. The Quick Start Guide places some functionality in the Pawn class. We will do this in the PlayerController instead.
    1. Using BeginPlay, check that we are not in the main menu, then spawn the HUD widget, and display it on screen (see figure 1).
    1. Optionally add a method to increase a score held by the Pawn.
    1. Use IsValid to check for an existing PauseMenu reference, and if not present spawn one as with the main menu. Set input mode to UI only and add the pause menu to viewport. Pause the game and re-enable the mouse cursor (see figure 2).
1. In the Graph section of the MainMenu Widget, create OnClicked events for each button (see figure 3).
1. In the Graph section of the PauseMenu Widget, creat OnClicked events for each button (see figure 4).
1. Create a MainMenu map which the Main Menu will run from, and set it as the default map in Project Settings > Maps and Modes. In the level Blueprint, use the same Create Widget + Add to Viewport method to generate the main menu, and also show the mouse cursor.

### What we found out

Prior to the UMG UI Designer, Slate UI was used to make user interfaces. Slate UI appears to have been a code based indented language for creating nested UI elements (something akin to HTML), and then rendering them within the game.

The UMG UI Designer, however, is a visual alternative to this, which allows for live positioning and visualisation of UI elements in a far more intuitive way. Elements are still nested, but done so with a click and drag system, and these elements have several attributes depending on their variety which can be adjusted directly or bound to variables/functions to determine the values of their attributes throughout the game. In comparison to Slate UI, it appears to be a far more intuitive solution and, as such allows much faster ideation.

HUDs and Menus can be created as Widget objects which fall under the UMG UI Designer system, allowing heavy customisation to fit a given game's requirements, and these complete objects can then be activated quite simply by adding/removing them from the viewport. In the case of main menus, this can optionally be run in a separate scene before loading into a game scene based on buttons in the menu.

Figures 1 and 2 below demonstrate the activation of the HUD and Pause Menu, using the Create Widget and Add to Viewport nodes alongside Widget references to spawn and maintain these objects.

A brief difficulty was had in attempting to access variables made in the C++ Pawn class, but the solution, as noted in the `Blueprints, Creating Variables in C++ For Use In BP` resource, was simply to add `BlueprintReadWrite, Category=MyCategory` in the `UPROPERTY` declaration.

This has by far been the most straightforward spike to manage, with well detailed guides available to see how these parts of UE4 work.

### Open Issues/Risks

1. While not a true issue, I was unable to resize the pause menu elements to match the main menu elements. My suspicion is that this has to do with the vertical box that holds them being contained within a border element, but it is unclear as to how to resolve this, as the border element is desired to partially hide the background.
    * A potential fix may be sitting the vertical box as a separate child of the canvas panel element, as in theory ZOrder alone would be enough to manage things properly. **In fact, it was simple enough to test upon writing, and has indeed worked to resolve the issue.

### Appendix

Figure 1: The BeginPlay script which activates the HUD provided we aren't in the main menu.

![PlayerController BeginPlay][PlayerController01]

Figure 2: Key events F and M, which increase the players score and activate the pause menu respectively.

![PlayerController ScoreBoost and PauseMenu][PlayerController02]

Figure 3: All the OnClicked events for Main Menu, including the Options Menu. Handles first level loading, switching to and from the options menu, adjusting screen resolution between presets, and exiting from the game.

![MainMenu OnClicked events][MainMenu01]

Figure 4: All OnClicked events for the Pause Menu. In this case Quit will bring us back to the Main Menu, and Return will deactivate the Pause Menu and return the player into the game.

![PauseMenu OnClicked events][PauseMenu01]

[PlayerController01]: https://monosnap.com/file/FBFx1hO33uWBhQpZzexC5ggMqYGcPR.png "PlayerController BeginPlay"
[PlayerController02]: https://monosnap.com/file/9lz81ybVCXPTq2QLq0ce6Wm1o4Aj7V.png "PlayerController ScoreBoost and PauseMenu"
[MainMenu01]: https://monosnap.com/file/4xJsOCDOqULK075CIjAbX2LZBVZ6Y5.png "MainMenu OnClicked events"
[PauseMenu01]: https://monosnap.com/file/fhW9Rf58ozmximk50GJSUeAByQuLNP.png "PauseMenu OnClicked events"