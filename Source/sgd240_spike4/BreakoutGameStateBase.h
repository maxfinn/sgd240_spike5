// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameStateBase.h"
#include "BreakoutGameStateBase.generated.h"

/**
 * 
 */
UCLASS()
class SGD240_SPIKE4_API ABreakoutGameStateBase : public AGameStateBase
{
	GENERATED_BODY()

	FTimerHandle TimeUpdateHandle;

public:
	ABreakoutGameStateBase();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Time)
	int32 GameTime;

protected:
	virtual void BeginPlay();

public:
	void UpdateGameTime();

};
