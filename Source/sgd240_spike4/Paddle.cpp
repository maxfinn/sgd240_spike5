// Fill out your copyright notice in the Description page of Project Settings.

#include "sgd240_spike4.h"
#include "Paddle.h"

// Sets default values
APaddle::APaddle()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Paddle size in centimetres
	PaddleSize = FVector(25.0f, 150.0f, 25.0f);
	MovementVelocity = 500.0f;
	PlayerScore = 0;

	// Our root component will be a box that reacts to physics
	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("RootComponent"));
	RootComponent = BoxComponent;
	BoxComponent->InitBoxExtent(PaddleSize / 2.0f);
	BoxComponent->SetCollisionProfileName(TEXT("Pawn"));

	// Create and position a mesh component for the paddle
	BoxVisual = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualRepresentation"));
	BoxVisual->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> BoxVisualAsset(TEXT("/Game/Cube.Cube"));
	if (BoxVisualAsset.Succeeded())
	{
		BoxVisual->SetStaticMesh(BoxVisualAsset.Object);
		BoxVisual->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
		BoxVisual->SetWorldScale3D(PaddleSize / 100.0f);
	}

}

// Called when the game starts or when spawned
void APaddle::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APaddle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APaddle::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

/** */
void APaddle::MoveY(float DeltaTime, float MovementMagnitude)
{
	FVector NewLocation = GetActorLocation() + FVector(0.0f, MovementMagnitude * MovementVelocity * DeltaTime, 0.0f);
	NewLocation.Y = FMath::Clamp(NewLocation.Y, -425.f, 425.0f);
	SetActorLocation(NewLocation);

}

void APaddle::BoostScore()
{
	PlayerScore++;
}

int32 APaddle::GetScore()
{
	return PlayerScore;
}