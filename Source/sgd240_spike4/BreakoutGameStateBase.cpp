// Fill out your copyright notice in the Description page of Project Settings.

#include "sgd240_spike4.h"
#include "BreakoutGameStateBase.h"

ABreakoutGameStateBase::ABreakoutGameStateBase()
{	
	GameTime = 0;

}

void ABreakoutGameStateBase::BeginPlay()
{
	GetWorldTimerManager().SetTimer(TimeUpdateHandle, this, &ABreakoutGameStateBase::UpdateGameTime, 1.0f, true, 0.0f);

}

void ABreakoutGameStateBase::UpdateGameTime()
{
	GameTime = (uint32)GetServerWorldTimeSeconds();

}